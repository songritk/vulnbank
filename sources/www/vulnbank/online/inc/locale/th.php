<?php
    define("ABOUT", "เกี่ยวกับ");
    define("ACCOUNT", "หมายเลขบัญชี");
    define("ACCESSIBILITY", "Accessibility");
    define("AMOUNT", "จำนวนเงิน");
    define("BALANCE", "เงินคงเหลือ");
    define("BIRTHDATE", "วันเกิด");
    define("CODE", "รหัส SMS");
    define("COMMENT", "ข้อเสนอแนะ");
    define("COMMENT_DESCRIPTION", "ส่งข้อความให้ผู้รับ");
    define("CREATE_USER", "สร้างผู้ใช้ใหม่");
    define("CREDITCARD", "หมายเลขบัตรเครดิต");
    define("DEPOSIT", "ฝาก");
    define("DATE", "วันที่");
    define("FIRSTNAME", "ชื่อ");
    define("FREE", "ฟรี");
    define("INCOME", "เงินได้");
    define("LASTNAME", "นามสกุล");
    define("OUTCOME", "รายจ่าย");
    define("PASSWORD", "รหัสผ่าน");
    define("PASSWORD_OLD", "ป้อนรหัสเดิม");
    define("PASSWORD_NEW", "ป้อนรหัสใหม่");
    define("PASSWORD_CONFIRM", "ป้อนยืนยันรหัสใหม่");
    define("PHONE", "หมายเลขโทรศัพท์");
    define("PORT", "พอร์ต");
    define("PROFILE_EDIT", "แก้ไขข้อมูลผู้ใช้");
    define("ROLE", "กฏ");
    define("RESETDB", "รีเซตฐานข้อมูล");
    define("RESPONSE", "ตอบกลับ");
    define("RESPONSE_TIME", "เวลา");
    define("SEND", "ส่ง");
    define("SERVICE", "บริการ");
    define("STATE", "สถานะ");
    define("TOTAL", "รวม");
    define("TRANSACTION", "การทำรายการ");
    define("UPDATE", "อัพเดท");
    define("USER", "ผู้ใช้");
    define("USED", "ใช้แล้ว");
    define("WITHDRAW", "ถอน");

    define("HISTORY_FAILED", "ลบการทำรายการ");
    define("HISTORY_CANCEL", "ยกเลิกรายการไม่สมบูรณ์");

    define("LOGIN_LOGIN", "ล็อกอิน");
    define("LOGIN_REGISTER", "ลงทะเบียน");
    define("LOGIN_RESET", "รีเซตรหัสผ่าน");

    define("MENU_STATISTICS", "สถิติ");
    define("MENU_TRANSACTIONS", "การทำรายการ");
    define("MENU_HISTORY", "ประวัติ");
    define("MENU_USERS", "ผู้ใช้");
    define("MENU_SYSTEM", "ระบบ");
    define("MENU_SETTINGS", "การตั้งค่า");
    define("MENU_WELCOME", "ยินดีต้อนรับ, ");
    define("MENU_USERINFO", "ข้อมูลผู้ใช้");
    define("MENU_LOGOUT", "ออก");

    define("PORTAL_DURATION", "For last 10 months");
    define("PORTAL_TRANSACTIONS", "Transactions history");
    define("PORTAL_TRANSACTIONS_DURATION", "Last 10 transactions");

    define("SETTINGS_NEXMO_API_KEY", "Nexmo API key");
    define("SETTINGS_NEXMO_API_SECRET", "Nexmo API secret");
    define("SETTINGS_SMS_API", "SMS API");
    define("SETTINGS_UPLOAD_PATH", "Upload path");
    define("SETTINGS_VB_API", "VulnBank API");
    define("SETTINGS_VB_OTP", "One Time Passwords");

    define("TRANSACTION_ACCOUNT", "Account");
    define("TRANSACTION_RECIPIENT", "Recipient");

    define("MSG_ACCESS_DENIED", "Permission denied");
    define("MSG_DBRESET", "Database reset successfully");
    define("MSG_CODE_INVALID", "Code verification failed");
    define("MSG_CODE_CHANGED", "Code changed successfully");
    define("MSG_CODE_SENT", "Code sent successfully");
    define("MSG_IMAGE_UPLOAD_EXT_FAILED", "File extension is not allowed");
    define("MSG_IMAGE_UPLOAD_SUCCESS", "Upload successfully");
    define("MSG_LOGIN_FAILED", "Incorrect login or password");
    define("MSG_LOGIN_SUCCESS", "Login successfully");
    define("MSG_PASS_UPDATE_SUCCESS", "Password update successfully");
    define("MSG_PASS_CHECK_FAIL", "Password validation failed");
    define("MSG_SETTINGS_UPDATE", "Settings update successfully");
    define("MSG_TRANSACTION_APPROVED", "%s transaction approved");
    define("MSG_TRANSACTION_CANCELED", "%s unfinished transactions canceled");
    define("MSG_TRANSACTION_FIND_FAIL", "Transaction not found");
    define("MSG_TRANSACTION_LOWMONEY", "Not enough money on account");
    define("MSG_TRANSACTION_REMOVEFAILED", "Failed transactions removed successfully");
    define("MSG_TRANSACTION_SUCCESS", "Sent %s$ to %s user");
    define("MSG_TRANSACTION_VERIFY_FAIL", "Transaction verification failed");
    define("MSG_TRANSACTION_YOURSELF", "It is not allowed to transfer money to yourself");
    define("MSG_USER_ADD_SUCCESS", "User %s added successfully");
    define("MSG_USER_FIND_FAIL", "User %s not found");
    define("MSG_USER_EXISTS", "User alreay exists");
    define("MSG_USER_UPDATE_SELF_SUCCESS", "Current user updated successfully");
    define("MSG_USER_UPDATE_SUCCESS", "User %s updated successfully");
    define("MSG_USER_REMOVE_SUCCESS", "User removed successfully");
    define("MSG_VALID_SUCCESS", "Validation successfully");
    define("MSG_VALID_FAIL", "Validation failed");
    define("MSG_VALID_PARAM_FAIL", "Validation of %s failed");

?>
