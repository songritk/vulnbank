# Dockerfile - VulnBank

FROM debian:stretch

LABEL maintainer="Songrit Kitisriworapan <songrit@npu.ac.th>"

COPY /sources/www /var/www
COPY /sources/evil /var/evil
COPY /nginx.config /etc/nginx/sites-enabled/default
COPY /start.sh /

RUN export DEBIAN_FRONTEND=noninteractive \
    && export TZ="Asia/Bangkok" \
    && apt-get update \
    && apt-get install -y --no-install-recommends \
        software-properties-common \
        gnupg2 \
        dirmngr \
        nginx \
        curl \
        php-fpm \
        php-mysql \
        php-curl \
        wget \
        libgomp1 \
        apt-utils \
        xz-utils \
        build-essential make \
        zsh \
    && chsh -s $(which zsh) \
    && mkdir -p /run/php \
    && chown www-data:www-data -R /var/www \
    && chown www-data:www-data -R /var/evil \
    && apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 0xF1656F24C74CD1D8 \
    && add-apt-repository 'deb [arch=amd64,i386,ppc64el] http://nyc2.mirrors.digitalocean.com/mariadb/repo/10.2/debian stretch main' \
    && DEBIAN_FRONTEND=noninteractive apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y mariadb-server \
    && wget -q https://ftp.osuosl.org/pub/blfs/conglomeration/ImageMagick/ImageMagick-6.8.9-1.tar.xz -O ImageMagick-6.8.9-1.tar.xz \
    && tar xf ImageMagick-6.8.9-1.tar.xz \
    && cd ImageMagick-6.8.9-1 \
    && ./configure; make ; make install; cd .. \
    && rm -rf imagemagick_6.8.9-1* \
    && apt remove -y build-essential make xz-utils \
    && ldconfig /usr/local/lib \
    && rm -fr /var/cache/apt/archives/* \
    && chmod +x /start.sh

CMD ["/start.sh"]
