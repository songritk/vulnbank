docker stop bank
docker rm bank
docker run -it -d --name bank \
	-p 8012:80 -d  \
        -v /home/songrit/docker/vulnbank/sources/www:/var/www \
        -v /home/songrit/docker/vulnbank/sources/evil:/var/evil \
	--mount type=bind,source="$(pwd)"/nginx.config,target=/etc/nginx/sites-enabled/default \
	songritk/vulnbank
